/*Задание No1. Переменные
1. Объявите две переменные: admin и name.
2. Запишите свое имя в переменную name.
3. Скопируйте значение из переменной name в admin.
4. Выведите на экран значение admin, используя функцию alert.
 */
let admin;
let name;
name = 'Oleg';
admin = name;
alert(admin);
/*Задание No2. Константы
Есть следующий код:
const birthday = '18.04.1982';
const age = someCode(birthday);
В нем объявлены константа birthday, а также age, которая вычисляется при помощи
некоторого кода, используя значение из birthday (в данном случае детали не имеют
значения, поэтому код не рассматривается).
Можно ли использовать заглавные буквы для имени birthday? А для age? Или
одновременно для обеих переменных?
const BIRTHDAY = '18.04.1982'; // использовать заглавные буквы? - yes,we can
const AGE = someCode(BIRTHDAY); // а здесь? тут так же можем

/*
 */
/*
Задание No3. Строки
Что выведет этот скрипт:
5. let name = "Ilya";
6. alert( `hello ${1}` ); // ?
7. alert( `hello ${"name"}` ); // ?
8. alert( `hello ${name}` ); // ?
 */
let name = "Ilya";
alert(`hello ${1}`); // hello 1
alert(`hello ${"name"}`); // hello name
alert(`hello ${name}`); // hello ilya

/*
Задание No4. Преобразования типов
Какой результат будет у выражений ниже?
"" + 1 + 0 // 10
"" - 1 + 0 // -1
true + false // 1
6 / "3" // 2
"2" * "3" // 6
4 + 5 + "px" // 9px
"$" + 4 + 5 // $45
"4" - 2 // 2
"4px" - 2 // NaN
7 / 0 // Infinity
" -9 " + 5 //  -9 5
" -9 " - 5 // -14
null + 1 // 1
undefined + 1 // NaN
 */

/*
Задание No6. Результат присваивания
Чему будут равны переменные a и x в примере ниже?
let a = 2;
let x = 1 + (a *= 2);
 */
let a = 2; //2
let x = 1 + (a *= 2); //5
alert(x);
/*
Задание No7. Операторы сравнения
Каким будет результат этих выражений?
5 > 4 // true
"ананас" > "яблоко" //false
"2" > "12" //true
undefined == null //true
undefined === null //false
null == "\n0\n" //false
null === +"\n0\n" false
 */

/*
Задание No8. Операторы взаимодействия
Создайте страницу, которая спрашивает имя у пользователя и выводит его.
 */
let yourName = prompt('Your name?');
alert(yourName);
/*
Задание No9. Условные операторы
Выведется ли alert? - ДА
if ("0") {
alert( 'Привет' );
}
 */
/*
Задание No10. Условные операторы
Используя конструкцию if..else, напишите код, который будет спрашивать: „Каково
«официальное» название JavaScript?“
Если пользователь вводит «ECMAScript», то показать: «Верно!», в противном случае –
отобразить: «Не знаете? ECMAScript!»
 */
let question = prompt('What is\n' +
    'The "official" name for JavaScript? ');
let answer = 'ECMAScript';
if (question === answer) {
    alert('Right');
} else {
    alert('Do not know? ECMAScript!')
}
/*
Задание No11. Покажите знак числа
Используя конструкцию if..else, напишите код, который получает число через prompt, а
затем выводит в alert:
1, если значение больше нуля,
-1, если значение меньше нуля,
0, если значение равно нулю.
Предполагается, что пользователь вводит только числа.
 */
let enterNumber = prompt('Please,enter the number');
if (enterNumber > 0) {
    alert(1);
} else if (enterNumber < 0) {
    alert(-1);
} else if (enterNumber == 0) {
    alert(0);
}
/*
Задание No12. ИЛИ
Что выведет код ниже?
alert( null || 2 || undefined ); //2
 */
/*
Задание No13. ИЛИ
Что выведет код ниже?
alert( alert(1) || 2 || alert(3) ); //1,then 2
 */
/*
Задание No14. И
Что выведет код ниже?
alert( 1 && null && 2 ); //null
 */
/*
Задание No15. И
Что выведет код ниже?
alert( alert(1) && alert(2) ); //1, then undefined
 */
/*
Задание No16.
Что выведет код ниже?
alert( null || 2 && 3 || 4 ); //3
 */
/*
Задание No17. Проверка значения из диапазона
Напишите условие if для проверки, что переменная age находится в диапазоне между
14 и 90 включительно.
«Включительно» означает, что значение переменной age может быть равно 14 или 90.
 */
let age = prompt('Enter your age');
if (age >= 14 && age <= 90) {
    alert('yes');
} else {
    alert('no');
}

/*
Задание No18. Проверка значения вне диапазона
Напишите условие if для проверки, что значение переменной age НЕ находится в
диапазоне 14 и 90 включительно.
Напишите два варианта: первый с использованием оператора НЕ !, второй – без этого
оператора.
 */
let age = prompt('Enter your age');
if (!(age >= 14 && age <= 90)) {
    alert('yes');
} else {
    alert('no');
}
let age = prompt('Enter your age');
if (age < 14 && age > 90) {
    alert('yes');
} else {
    alert('no');
}
/*
Задание No19. If
Какие из перечисленных ниже alert выполнятся?
Какие конкретно значения будут результатами выражений в условиях if(...)?
if (-1 || 0) alert( 'first' ); //this
if (-1 && 0) alert( 'second' );
if (null || -1 && 1) alert( 'third' ); //and this
 */
/*
Задание No20. Проверка логина
Напишите код, который будет спрашивать логин с помощью prompt.
Если посетитель вводит «Админ», то prompt запрашивает пароль, если ничего не
введено или нажата клавиша Esc – показать «Отменено», в противном случае
отобразить «Я вас не знаю».
Пароль проверять так:
1. Если введён пароль «Я главный», то выводить «Здравствуйте!»,
2. Иначе – «Неверный пароль»,
3. При отмене – «Отменено».
Блок-схема:

Для решения используйте вложенные блоки if. Обращайте внимание на стиль и
читаемость кода.
Подсказка: передача пустого ввода в приглашение prompt возвращает пустую строку ''.
Нажатие клавиши Esc во время запроса возвращает null.
 */
let enterLogin = prompt('Enter your login');
if (enterLogin == 'Admin') {
    let enterPassword = prompt('Enter your password');
    if (enterPassword == 'I\'m in charge') {
        alert('Hello');
    } else if (enterPassword == '') {
        alert('Canceled');
    } else if (enterPassword == null) {
        alert('Canceled');
    } else {
        alert('Wrong password');
    }

} else if (enterLogin == '') {
    alert('Canceled');
} else if (enterLogin == null) {
    alert('Canceled');
} else {
    alert('I do not know you');
}
/*
Задание No21. Переписать условия "if" на "switch"
Перепишите код с использованием одной конструкции switch:
let a = +prompt('a?', '');
if (a == 0) {
alert( 0 );
}
if (a == 1) {
alert( 1 );
}
if (a == 2 || a == 3) {
alert( '2,3' );
}
 */
let a = +prompt('a?', '');
switch (a) {
    case 0:
        console.log(0);
        break;
    case 1:
        console.log(1);
        break;
    case 2:
    case 3:
        console.log('2,3');
        break;
}
/*
Задание No22.
Дано целое число. Если оно является положительным, то прибавить к нему 1; в
противном случае не изменять его. Вывести полученное число.
 */
let intNumber = prompt('Enter number');
if (intNumber > 0) {
    intNumber++;
    console.log(intNumber);
} else {
    console.log(intNumber);
}
/*
Задание No23.
Дано целое число. Если оно является положительным, то прибавить к нему 1; в
противном случае вычесть из него 2. Вывести полученное число.
 */
let intNumberAgain = prompt('Enter number');
if (intNumberAgain > 0) {
    intNumberAgain++;
    console.log(intNumberAgain);
} else {
    console.log(intNumberAgain - 2);
}
/*
Задание No24.
Дано целое число. Если оно является положительным, то прибавить к нему 1; если
отрицательным, то вычесть из него 2; если нулевым, то заменить его на 10. Вывести
полученное число.
 */
let intNumberExercise = prompt('Enter number');
if (intNumberExercise > 0) {
    intNumberExercise++;
    console.log(intNumberExercise);
} else if (intNumberExercise < 0) {
    console.log(intNumberExercise - 2);
} else if (intNumberExercise == 0) {
    intNumberExercise = 10;
    console.log(intNumberExercise);
}
/*
Задание No25.
Даны три целых числа. Найти количество положительных чисел в исходном наборе.
 */
let enterNumbers = [
    prompt('Число а'),
    prompt('Число b'),
    prompt('Число c')
];
let positiveNumbers = 0;

for (let i = 0; i < 3; i++) {
    if (enterNumbers[i] >= 0 && enterNumbers[i] !== null) {
        positiveNumbers++;
    }
}


/*
Задание No27.
Даны два числа. Вывести большее из них.
 */
let firstNumber = prompt('Enter number');
let secondNumber = prompt('Enter number');
if (firstNumber > secondNumber) {
    console.log(firstNumber ?? secondNumber);
} else if (secondNumber > firstNumber) {
    console.log(secondNumber ?? firstNumber);
}
/*
Задание No28.
Даны два числа. Вывести вначале большее, а затем меньшее из них.
 */
let firstNumber = prompt('Enter number');
let secondNumber = prompt('Enter number');
if (firstNumber > secondNumber) {
    console.log(firstNumber ?? secondNumber);
} else if (secondNumber > firstNumber) {
    console.log(secondNumber ?? firstNumber);
}

if (firstNumber < secondNumber) {
    console.log(firstNumber ?? secondNumber);
} else if (secondNumber < firstNumber) {
    console.log(secondNumber ?? firstNumber);
}
/*
Задание No29. Даны две переменные вещественного типа: A, B. Перераспределить
значения данных переменных так, чтобы в A оказалось меньшее из значений, а в B —
большее. Вывести новые значения переменных A и B.
 */
let A = prompt('Number A');
let B = prompt('Number B');
if (A > B) {
    let temp = A;
    A = B;
    B = temp;
}
console.log('A = ' + A);
console.log('B = ' + B);
/*
Задание No30.
Даны две переменные целого типа: A и B. Если их значения не равны, то присвоить
каждой переменной сумму этих значений, а если равны, то присвоить переменным
нулевые значения. Вывести новые значения переменных A и B.
 */
let A = +prompt('Number A');
let B = +prompt('Number B');
if (A == B) {
    A = B = A + B;
} else {
    A = B = 0;
}
console.log('A = ' + A);
console.log('B = ' + B);
/*
Задание No31.
Даны две переменные целого типа: A и B. Если их значения не равны, то присвоить
каждой переменной большее из этих значений, а если равны, то присвоить
переменным нулевые значения. Вывести новые значения переменных A и B.
 */
let A = +prompt('Number A');
let B = +prompt('Number B');
if (A == B) {
    A = B = 0;
} else {
    A = B = A > B ? A : B;
}
console.log('A = ' + A);
console.log('B = ' + B);
/*
Задание No32.
Даны три числа. Найти наименьшее из них.
 */
let enterNumbers = [
    +prompt('Number a'),
    +prompt('Number b'),
    +prompt('Number c')
];
let maxNumber = enterNumbers[0];
for (let i = 1; i < 3; i++) {
    if (maxNumber < enterNumbers[i]) {
        maxNumber = enterNumbers[i];
    }
}
console.log(maxNumber);
/*
Задание No33.
Даны три числа. Найти среднее из них (то есть число, расположенное между
наименьшим и наибольшим).
 */
let number1 = +prompt('Number a');
let number2 = +prompt('Number b');
let number3 = +prompt('Number c');

if ((number1 > number2 && number1 < number3) || (number1 < number2 && number1 > number3)) {
    console.log(number1);
} else if ((number2 > number1 && number2 < number3) || (number2 < number1 && number2 > number3)) {
    console.log(number2);
} else if ((number3 > number1 && number3 < number2) || (number3 < number1 && number3 > number2)) {
    console.log(number3);
}
/*
Задание No34.
Даны три числа. Вывести вначале наименьшее, а затем наибольшее из данных чисел.
 */
let enterNumbers = [
    +prompt('Число а'),
    +prompt('Число b'),
    +prompt('Число c')
];
let maxNumber = enterNumbers[0];
let minNumber = enterNumbers[0];

for (let  i = 1; i < 3; i++) {
    if (maxNumber < enterNumbers[i]) {
        maxNumber = enterNumbers[i];
    } else if (minNumber > enterNumbers[i]) {
        minNumber = enterNumbers[i];
    }
}

alert('Max of numbers = ' + maxNumber);
alert('Min of numbers = ' + minNumber);

/*
Даны три числа. Найти сумму двух наибольших из них.
 */
let first = 1;
let second = 2;
let thid = 3;
if (second >= first && thid >= first) {
    console.log(second + thid)
} else if (first >= second && thid >= second) {
    console.log(first + thid)
} else console.log(first + second);
/*
Задание No36.
Даны три целых числа, одно из которых отлично от двух других, равных между собой.
Определить порядковый номер числа, отличного от остальных.
 */
let first = +prompt('Число а');
let second =  +prompt('Число b');
let third = +prompt('Число c');

if (first === second) {+
    console.log(third);
} else if (first === third) {
    console.log(second);
} else if (second === third) {
    console.log(first);
}